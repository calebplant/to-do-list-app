# To-Do List App

## Quick Demo

[See the full demo video here.](https://www.youtube.com/watch?v=e_Xcav-qMrk)

### Some Screenshots

#### App Dashboard

![App Dashboard](media/dashboard.png)

#### Creating a Todo

![Create Todo](media/CreateTodo.png)

#### Deleting a Todo

![Delete Todo](media/DeleteTodo.png)

#### Reminder Email

![Reminder Email](media/ReminderEmail.png)


## Features

* Todo creation/editing/deletion

* Automatic todo sorting

* Automatic reminder emails

* Drag and drop todos