import { LightningElement, api } from 'lwc';

export default class TodoCard extends LightningElement {
    
    @api todo;

    get toggleCompleteLabel() {
        let result = 'Mark as ';
        result += this.todo.isComplete ? 'Incomplete' : 'Complete';
        return result;
    }

    get dateLabel() {
        if(!this.todo.displayDate) {
            return '';
        } else if(this.todo.isComplete) {
            return 'Done: ' + this.todo.displayDate;
        } else {
            return 'Due: ' + this.todo.displayDate;
        }
    }

    // Handlers
    handleCardClick() {
        console.log('todoCard :: handleCardClick');
        // console.log(JSON.parse(JSON.stringify(this.todo.id)));
        
        this.dispatchEvent(new CustomEvent('cardclick', {
            detail: this.todo.id
        }));
    }

    handleDragStart(event) {
        console.log('todoCard :: handleDragStart');
        event.dataTransfer.setData("text", this.todo.id);
    }

    handleDeleteTodo() {
        console.log('todoCard :: handleDeleteTodo');
        this.dispatchEvent(new CustomEvent('deletecard', {
            detail: this.todo.id
        }));
    }

    handleToggleComplete() {
        console.log('todoCard :: handleSetComplete');
        let newValue = this.todo.isComplete ? false : true;
        this.dispatchEvent(new CustomEvent('togglecomplete', {
            detail: {recordId: this.todo.id, updatedValue: newValue}
        }));
    }

    handleDoNothing(event) {
        console.log('todoCard :: handleDoNothing');
        event.stopPropagation(); // Prevent parent's onclick div from propogating
    }
    
}