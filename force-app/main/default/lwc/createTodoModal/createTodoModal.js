import { LightningElement, api } from 'lwc';

export default class CreateTodoModal extends LightningElement {
    
    @api objectApiName;

    handleSuccess(event) {
        console.log('createTodoModal :: handleSuccess');
        // console.log(event.detail);  
        // console.log(JSON.parse(JSON.stringify(event.detail)));  
        this.dispatchEvent(new CustomEvent('createtodo', {detail: event.detail}));
    }

    handleCancel(event) {
        console.log('createTodoModal :: handleCancel');
        this.dispatchEvent(new CustomEvent('cancelcreate'));
        const inputFields = this.template.querySelectorAll(
            'lightning-input-field'
        );
        if (inputFields) {
            inputFields.forEach(field => {
                field.reset();
            });
        }
    }

    handleSubmit() {
        console.log('createTodoModal :: handleSubmit');
        const todoForm = this.template.querySelector('lightning-record-edit-form');
        todoForm.submit();
    }
}