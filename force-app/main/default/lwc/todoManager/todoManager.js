import { LightningElement, wire } from 'lwc';
import { refreshApex } from '@salesforce/apex';
import { deleteRecord, updateRecord } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getUserToDos from '@salesforce/apex/ToDoListController.getUserToDos';
import ISCOMPLETED_FIELD from '@salesforce/schema/Todo__c.Completed__c';
import ID_FIELD from '@salesforce/schema/Todo__c.Id';

export default class TodoManager extends LightningElement {

    todos = [];
    selectedTodoId;
    error;
    _recordResponse;
    prepopulatedFields;

    createModalVisible = false;
    editModalVisible = false;
    deleteModalVisible = false;    

    @wire(getUserToDos)
    wiredTodos(response)
    {
        console.log('todoManager :: wiredTodos');

        this._recordResponse = response;
        if(response.data) {
            console.log("Size retrieved from server: ", response.data.length);
            console.log(response.data);
            // this.distributeTodosToLists(response.data);
            this.todos = response.data;
        } else if (response.error) {
            console.log("Error fetching user todos!");
            console.log(response.error);
            this.error = response.error;
        }
    }

    get upcomingTodos() {
        return this.todos.filter(eachTodo => {
            return (!eachTodo.isOverdue && !eachTodo.isDueToday) && !eachTodo.isComplete
        });
    }

    get dueTodayTodos() {
        return this.todos.filter(eachTodo => {
            return (!eachTodo.isOverdue && eachTodo.isDueToday) && !eachTodo.isComplete
        });
    }

    get overdueTodos() {
        return this.todos.filter(eachTodo => {
            return eachTodo.isOverdue && !eachTodo.isComplete
        });
    }

    get completedTodos() {
        return this.todos.filter(eachTodo => {
            return eachTodo.isComplete
        });
    }

    connectedCallback() {
        this.resetParams();
    }

    // Handlers
    handleCardClick(event) {
        console.log('todoManager :: handleCardClick');
        // console.log(event.detail);
        if(event.detail) {
            this.selectedTodoId = event.detail;
            this.showEditModal();
        }
    }

    handleCreateTodo(event) {
        console.log('todoManager :: handleCreateTodo');
        
    }

    handleEditTodo(event) {
        console.log('todoManager :: handleEditTodo');
        refreshApex(this._recordResponse);
        this.hideEditModal();
        this.resetParams();
    }

    handleCardDelete(event) {
        console.log('todoManager :: handleCardDelete');
        if(event.detail) {
            this.selectedTodoId = event.detail;
            this.showDeleteModal();
        }
    }

    handleDeleteTodo() {
        console.log('todoManager :: handleDeleteTodo');
        this.hideDeleteModal();
        // console.log('lets delete: ');
        // console.log(this.selectedTodoId);
        
        deleteRecord(this.selectedTodoId).then(() => {
            console.log('Successfully deleted todo: ' + this.selectedTodoId);
            refreshApex(this._recordResponse);
        }).catch(error => {
            console.log('Error deleting todo!');
            console.log(error);
        }).finally(() => {
            this.resetParams();
        });
    }

    handleCreateTodo(event) {
        console.log('todoManager :: handleCreateTodo');
        this.showCreateModal();
    }

    handleTodoCreated(event) {
        console.log('todoManager :: handleTodoCreated');
        this.hideCreateModal();
        refreshApex(this._recordResponse);
    }

    handleCancelCreate(event) {
        console.log('todoManager :: handleCancelCreate');
        this.hideCreateModal();
    }

    handleCancelDelete() {
        console.log('todoManager :: handleCancelDelete');
        this.hideDeleteModal();
        this.resetParams();
    }

    handleCancelEdit() {
        console.log('todoManager :: handleCancelEdit');
        this.hideEditModal();
        this.resetParams();
    }

    handleToggleComplete(event) {
        console.log('todoManager :: handleToggleComplete');
        let todoId = event.detail.recordId;
        let updatedValue = event.detail.updatedValue;
        // console.log('todoId: ');
        // console.log(todoId);
        // console.log('updatedValue: ');
        // console.log(updatedValue);
        this.setTodoCompletedStatus(todoId, updatedValue);
    }

    handleCardDrop(event) {
        console.log('todoManager :: handleCardDrop');
        // console.log(event.currentTarget.id);
        let targetList = event.currentTarget.id.split('-')[0]; //example id: "upcoming-36" ... we want "upcoming"
        let draggedTodoId = event.dataTransfer.getData("text");
        
        switch(targetList) {
            case 'upcoming':
            case 'today':
            case 'overdue':
                console.log('not completed');
                this.editOnDragAndDrop(targetList, draggedTodoId);
                break;
            case 'completed':
                console.log('completed');
                this.setTodoCompletedStatus(draggedTodoId, true);
                break;
        }
    }
    
    handleDragOver(event) {
        event.preventDefault();
    }



    // Utils
    resetParams() {
        this.selectedTodoId = null;
        this.prepopulatedFields = {
            dueDate: '',
            isComplete: ''
        };
    }

    setTodoCompletedStatus(todoId, status) {
        const fields = {};
        fields[ID_FIELD.fieldApiName] = todoId;
        fields[ISCOMPLETED_FIELD.fieldApiName] = status;
        const recordInput = {fields};

        updateRecord(recordInput)
            .then(() => {
                return refreshApex(this._recordResponse);
            })
            .catch(error => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error updating todo',
                        message: error.body.message,
                        variant: 'error'
                    })
                );
            });
    }

    editOnDragAndDrop(listType, draggedTodoId) {
        if(listType === 'upcoming' || listType === 'overdue') {
            this.prepopulatedFields.isComplete = false;
        } else if(listType === 'today') {
            let todayAtMidnight = new Date();
            todayAtMidnight.setHours(24,0,0,0);
            this.prepopulatedFields.dueDate = todayAtMidnight.toISOString();
            this.prepopulatedFields.isComplete = false;
        }

        this.selectedTodoId = draggedTodoId;
        this.showEditModal();
    }

    showCreateModal() {
        this.createModalVisible = true;
    }

    hideCreateModal() {
        this.createModalVisible = false;
    }
    showEditModal() {
        this.editModalVisible = true;
    }

    hideEditModal() {
        this.editModalVisible = false;
    }

    showDeleteModal() {
        this.deleteModalVisible = true;
    }

    hideDeleteModal() {
        this.deleteModalVisible = false;
    }
}