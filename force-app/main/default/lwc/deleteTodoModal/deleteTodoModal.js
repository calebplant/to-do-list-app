import { LightningElement, api } from 'lwc';

export default class DeleteTodoModal extends LightningElement {

    @api objectApiName;
    @api todoId;

    // Handlers
    handleConfirmDelete() {
        console.log('deleteTodoModal :: handleConfirmDelete');
        this.dispatchEvent(new CustomEvent('confirmdelete'));
    }

    handleCancel() {
        console.log('deleteTodoModal :: handleCancel');
        this.dispatchEvent(new CustomEvent('canceldelete'));
    }
}