import { LightningElement, api } from 'lwc';

export default class EditTodoModal extends LightningElement {

    @api objectApiName;
    @api todoId;
    @api prepopulatedFields;

    connectedCallback() {
        console.log(JSON.parse(JSON.stringify(this.prepopulatedFields)));
        
    }

    get hasPrepopulatedDueDate() {
        return this.prepopulatedFields.dueDate !== '' ? true : false;
    }

    get hasPrepopulatedCompleteField() {
        return this.prepopulatedFields.isComplete !== '' ? true : false;
    }

    // Handlers
    handleSuccess(event) {
        console.log('editTodoModal :: handleSuccess');        
        this.dispatchEvent(new CustomEvent('editodo', {
            detail: event.detail
        }));
    }

    handleCancel() {
        console.log('editTodoModal :: handleCancel');        
        this.dispatchEvent(new CustomEvent('canceledit'));
    }

    handleSubmit() {
        console.log('editTodoModal :: handleSubmit');
        const todoForm = this.template.querySelector('lightning-record-edit-form');
        todoForm.submit();
    }
}