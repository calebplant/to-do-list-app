import { LightningElement, api } from 'lwc';

const SHOW_ADD_TODO_BUTTON_VARIANTS = ['Upcoming', 'Due Today', 'Completed']

export default class TodoList extends LightningElement {
    @api listName;
    @api listIcon;
    @api todos;

    get showAddTodoButton() {
        return SHOW_ADD_TODO_BUTTON_VARIANTS.includes(this.listName);
    }

    // Handlers
    handleCardClick(event) {
        console.log('todoList :: handleCardclick');
        // console.log(event.detail);
        let todoId = event.detail;
        this.dispatchEvent(new CustomEvent('cardclick', {
            detail: todoId
        }));
    }

    handleCardDelete(event) {
        console.log('todoList :: handleCardDelete');
        let todoId = event.detail;
        this.dispatchEvent(new CustomEvent('carddelete', {
            detail: todoId
        }));
    }

    handleToggleComplete(event) {
        console.log('todoList :: handleToggleComplete');
        // let todoId = event.detail.recordId;
        // let updatedValue = event.detail.updatedValue;
        this.dispatchEvent(new CustomEvent('togglecomplete', {
            detail: {recordId: event.detail.recordId,
                    updatedValue: event.detail.updatedValue}
        }));
    }

    handleCreateTodo(event) {
        console.log('todoList :: handleCreateTodo');
        this.dispatchEvent(new CustomEvent('createtodo'));
    }
}