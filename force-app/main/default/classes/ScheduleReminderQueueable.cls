public with sharing class ScheduleReminderQueueable implements Queueable{

    public List<Todo__c> todos;

    public ScheduleReminderQueueable(List<ToDo__c> passedTodos) {
        todos = passedTodos;
    }
    
    public void execute(QueueableContext context) {
        System.debug('START ScheduleReminderQueueable');
        System.debug('Todos: ');
        System.debug(todos);
        Reminder_Helper__c scheduledRun = [SELECT Id, Next_Scheduled_Run__c, Job_Id__c FROM Reminder_Helper__c LIMIT 1];
        Boolean updateReminders = false;
        Datetime nextRunTime = scheduledRun.Next_Scheduled_Run__c;

        for(Todo__c eachTodo : todos) {
            if(eachTodo.Reminder_Date__c != null) {
                if (eachTodo.Reminder_Date__c < nextRunTime) {
                    updateReminders = true;
                    nextRunTime = eachTodo.Reminder_Date__c;
                }
            }
        }

        if(updateReminders) {
            System.debug('Updating next run time...');
            try {
                System.abortJob(scheduledRun.Job_Id__c);
            } catch (Exception e) {
                System.debug('No job was scheduled!');
            }
            // Reschedule job
            ScheduledReminder s = new ScheduledReminder();
            String jobName = 'Self Schedule Reminder Service ' + nextRunTime.format('hh:mm');
            String cronString = ScheduleReminderService.buildCronFromDatetime(nextRunTime);
            System.debug('Scheduling job');
            String jobId = System.Schedule(jobName, cronString, s); 
            System.debug('Scheduled ' + jobId);
            // Update Reminder Helper
            scheduledRun.Next_Scheduled_Run__c = nextRunTime;
            scheduledRun.Job_Id__c = jobId;
            System.debug('Updating Schedule Helper');
            update scheduledRun;
            System.debug('Updated!');
        }
        // Unflag todo
        // newTodo.Update_Reminders__c = false;
        // update newTodo;
    }
}
