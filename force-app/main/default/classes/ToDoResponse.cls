public class ToDoResponse
{
    @AuraEnabled
    public Id id {get; set;}
    @AuraEnabled
    public String title {get; set;}
    @AuraEnabled
    public String displayDate {get; set;}
    @AuraEnabled
    public Boolean isOverdue {get; set;}
    @AuraEnabled
    public Boolean isDueToday {get; set;}
    @AuraEnabled
    public Boolean isComplete {get; set;}

    public ToDoResponse(Id recordId)
    {
        id = recordId;
    }

    public void setDueDate(Datetime d)
    {
        if(d == null) {
            isOverdue = false;
            isDueToday = false;
        } else {
            displayDate = d.format('MM/dd, hh:mm');
            if(d < System.now()) {
                isOverdue = true;
                isDueToday = false;
            } else if(d.date() == System.today()) {
                isOverdue = false;
                isDueToday = true;
            } else {
                isOverdue = false;
                isDueToday = false;
            }
        }
    }

    public void setCompletedDate(Datetime d)
    {
        isOverdue = false;
        isDueToday = false;

        if(d != null) {
            displayDate = d.format('MM/dd, hh:mm');
        }
    }
}
