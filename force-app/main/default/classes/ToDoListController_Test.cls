@isTest
public with sharing class ToDoListController_Test {

    private static String UPCOMING_TODO_TITLE = 'upcoming';
    private static String DUE_TODAY_TODO_TITLE = 'due today';
    private static String OVERDUE_TODO_TITLE = 'overdue';
    private static String COMPLETED_TODO_TITLE = 'completed';
    
    @TestSetup
    static void makeData(){
        List<Todo__c> todos = new List<Todo__c>();
        todos.add(new Todo__c(Title__c=UPCOMING_TODO_TITLE, Due_Date__c=System.now().addDays(5)));
        todos.add(new Todo__c(Title__c=DUE_TODAY_TODO_TITLE, Due_Date__c=System.now().addHours(1)));
        todos.add(new Todo__c(Title__c=OVERDUE_TODO_TITLE, Due_Date__c=System.now().addDays(-1)));
        todos.add(new Todo__c(Title__c=COMPLETED_TODO_TITLE, Due_Date__c=System.now().addDays(-1), Completed__c=True));
        insert todos;
    }

    @isTest
    static void placeholder()
    {

    }
}
