global class ScheduledReminder implements Schedulable{

    private Integer MINUTE_FUZZ = 3;
    private Datetime startTime;

    global void execute(SchedulableContext sc)
    {
        System.debug('START ScheduledReminder');
        startTime = System.now();
        // Datetime minTime = startTime.addMinutes(-1 * MINUTE_FUZZ);
        // Datetime maxTime = startTime.addMinutes(MINUTE_FUZZ);
        Datetime minTime = startTime.addDays(-1 * MINUTE_FUZZ);
        Datetime maxTime = startTime.addDays(MINUTE_FUZZ);
        System.debug('Get todos');
        List<ToDo__c> todosToSend = [SELECT Id, Title__c, Description__c, CreatedById, Reminder_Date__c
                                FROM ToDo__c
                                WHERE Reminder_Date__c > :minTime AND Reminder_Date__c < :maxTime];
        System.debug(todosToSend);
        System.debug('Try sending emails');
        sendReminderEmails(todosToSend);
        System.debug('Attempt self-reschedule');
        rescheduleSelf(sc);
    }

    private void sendReminderEmails(List<ToDo__c> todos)
    {
        // Group Todos by creator
        Map<Id, List<ToDo__c>> todosByCreatorId = new Map<Id, List<ToDo__c>>();
        for(ToDo__c eachTodo : todos) {
            System.debug(eachTodo);
            if(todosByCreatorId.get(eachTodo.CreatedById) != null) {
                List<ToDo__c> currentTodos = todosByCreatorId.get(eachTodo.CreatedById);
                currentTodos.add(eachTodo);
            } else {
                todosByCreatorId.put(eachTodo.CreatedById, new List<ToDo__c>{eachTodo});
            }
        }

        // Send todo list to appropriate user
        System.debug('Todos to send:');
        System.debug(todosByCreatorId);
        Map<Id, User> userById = new Map<Id, User>([SELECT Id FROM User WHERE Id IN :todosByCreatorId.keySet()]);
        List<Messaging.SingleEmailMessage> emailsToSend = new List<Messaging.SingleEmailMessage>();
        Id senderId = getSenderId();
        for(Id eachRecipientId : userById.keySet()) {
            System.debug('Generating email');
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            email.setTargetObjectId(eachRecipientId);
            email.setTreatTargetObjectAsRecipient(true);
            email.setSubject(buildSubject( todosByCreatorId.get(eachRecipientId) ));
            email.setPlainTextBody(buildBody( todosByCreatorId.get(eachRecipientId) ));
            email.setSaveAsActivity(false);
            if(senderId != null) {
                email.setOrgWideEmailAddressId(senderId);
            }
            emailsToSend.add(email);
            // String emailSubject = buildSubject(todosByCreatorId.get(eachUserId));
            // String emailBody = buildBody(todosByCreatorId.get(eachUserId));
        }
        System.debug('Sending emails');
        Messaging.sendEmail(emailsToSend);
        System.debug('Emails sent');
    }

    private void rescheduleSelf(SchedulableContext context) {
        Datetime nextNearestTime = startTime.addMinutes(MINUTE_FUZZ);
        Datetime nextReminderTime = [SELECT Id, Reminder_Date__c
                                    FROM ToDo__c
                                    WHERE Reminder_Date__c > :nextNearestTime
                                    ORDER BY Reminder_Date__c ASC
                                    LIMIT 1].Reminder_Date__c;
        ScheduledReminder s = new ScheduledReminder();
        String jobName = 'Self Schedule Reminder Service ' + nextNearestTime.format('hh:mm');
        String cronString = ScheduleReminderService.buildCronFromDatetime(nextReminderTime);
        System.debug('Scheduling...');
        System.abortJob(context.getTriggerId());
        String jobId = System.Schedule(jobName, cronString, s);
        System.debug('Scheduled ' + jobId);
        System.debug('Attempt update helper values');
        updateHelperValues(nextReminderTime, jobId);
    }

    private void updateHelperValues(Datetime nextRunTime, String jobId) {
        Reminder_Helper__c reminderHelper = [SELECT Id, Next_Scheduled_Run__c, Job_Id__c FROM Reminder_Helper__c LIMIT 1];
        reminderHelper.Next_Scheduled_Run__c = nextRunTime;
        reminderHelper.Job_Id__c = jobId;
        System.debug('Updating...');
        update reminderHelper;
        System.debug('Updated!');
    }

    private String buildSubject(List<ToDo__c> todos)
    {
        if(todos.size() > 1) {
            return 'Multiple Todo Reminders';
        } else {
            return 'Todo Reminder: ' + todos[0].Title__c;
        }
    }

    private String buildBody(List<ToDo__c> todos)
    {
        String body = 'This is a reminder that you scheduled for the following ToDos:\n\n\n';
        for(ToDo__c eachTodo : todos) {
            body += eachTodo.Title__c + '\nDue: ' + eachTodo.Reminder_Date__c.format() + '\n\n' + 
                     eachTodo.Description__c + '\n\n';
        }
        return body;
    }

    private Id getSenderId()
    {
        List<OrgWideEmailAddress> owea = [SELECT Id FROM OrgWideEmailAddress WHERE Address = 'cplant1776+system@gmail.com'];
        if ( owea.size() > 0 ) {
            return owea.get(0).Id;
        } else {
            return null;
        }
    }
}
