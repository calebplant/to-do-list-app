public with sharing class ScheduleReminderService {
    
    @InvocableMethod(label='Update Scheduled Reminders' description='Updates scheduled reminder emails, if needed.'category='ToDo__c')
    public static void scheduleReminder(List<ToDo__c> todos)
    {
        ScheduleReminderQueueable s = new ScheduleReminderQueueable(todos);
        String jobId = System.enqueueJob(s);
        System.debug('Enqueued: ' + jobId);
    }

    public static String buildCronFromDatetime(Datetime d)
    {
        return d.second() + ' ' + d.minute() + ' ' + d.hour() + ' ' + d.day() + ' ' + d.month() + ' ? ' + d.year();
    }
}
