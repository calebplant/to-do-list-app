public with sharing class ToDoListController {
    
    @AuraEnabled(cacheable=true)
    public static List<ToDoResponse> getUserToDos()
    {
        Datetime oneWeekAgo = System.now().addDays(-7);
        List<ToDo__c> todos = [SELECT Id, Title__c, Due_Date__c, Completed__c, Completed_Date__c
                               FROM ToDo__c
                               WHERE Completed_Date__c = null OR Completed_Date__c > :oneWeekAgo
                               ORDER BY Due_Date__c, Completed_Date__c DESC];
        return wrapToDos(todos);
    }

    private static List<ToDoResponse> wrapToDos(List<ToDo__c> todos)
    {
        List<ToDoResponse> response = new List<ToDoResponse>();

        for(ToDo__c eachTodo : todos) {
            ToDoResponse newToDoResponse = new ToDoResponse(eachTodo.Id);
            newToDoResponse.title = eachTodo.Title__c;
            newToDoResponse.isComplete = eachTodo.Completed__c;
            if(!newToDoResponse.isComplete) {
                newToDoResponse.setDueDate(eachTodo.Due_Date__c);
            } else {
                newToDoResponse.setCompletedDate(eachTodo.Completed_Date__c);
            }
            response.add(newToDoResponse);
        }
        return response;
    }
}
