public with sharing class ToDoTrigger_Helper {
    
    public static void afterUpdate(Map<Id, ToDo__c> oldValues, Map<Id, ToDo__c> newValues) {
        System.debug('old:' + oldValues);
        System.debug('new: ' + newValues);
        List<ToDo__c> changedReminderDateTodos = new List<ToDo__c>();
        for(ToDo__c eachTodo : newValues.values()) {
            if(oldValues.get(eachTodo.Id).Reminder_Date__c != eachTodo.Reminder_Date__c) {
                changedReminderDateTodos.add(eachTodo);
            }
        }
        if(changedReminderDateTodos.size() > 0) {
            ScheduleReminderService.scheduleReminder(changedReminderDateTodos);
        }
    }
}
