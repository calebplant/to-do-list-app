trigger ToDoTrigger on ToDo__c (after update) {
    System.debug('START ToDoTrigger');
    if(Trigger.isAfter && Trigger.isUpdate) {
        System.debug('START afterUpdate');
        ToDoTrigger_Helper.afterUpdate(Trigger.oldMap, Trigger.newMap);
    }
}